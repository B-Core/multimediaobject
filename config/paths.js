const path = require('path');

module.exports = {
  CONFIG: path.resolve(__dirname, '../config'),
  SRC: path.resolve(__dirname, '../src'),
  DIST: path.resolve(__dirname, '../dist'),
  LIBS: path.resolve(__dirname, '../src/libs'),
  SCHEMAS: path.resolve(__dirname, '../src/schemas'),
  TESTS: path.resolve(__dirname, '../__sources/'),
};